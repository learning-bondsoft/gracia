<!DOCTYPE html>
<html xml:lang="ru" lang="ru">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <meta name="keywords" content="Клиника Грация">
    <meta name="description" content="Клиника Грация">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=2" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/waves.css">
    <link rel="stylesheet" href="css/lazyframe.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/jquery.fancybox3.css">
    <link rel="stylesheet" href="css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/stepicons.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/modal-video.min.css">
    <link rel="stylesheet" href="css/iziModal.css">
    <link rel="stylesheet" href="css/icon-pack.css">
    <link rel="stylesheet" href="css/main.css?v2">
    <title>Клиника Грация</title>
</head>

<body>
    <div id="body-inner">
        <header id="sticker" class="mobile">
            <div class="container">
                <div class="row">
                    <a href="#my-menu" id="hamburger" class="hamburger hamburger--spin waves-effect waves-light">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </a>
                    <div class="logo col-lg-4 col-12 text-center align-self-center">
                        <a href="/"> <img class="img-fluid logo-desktop" src="images/logo-hr.png" alt="Грация"> </a>
                    </div>
                    <div class="col-lg-8 header__right">
                        <div class="actions" style="display: flex; justify-content: flex-end; align-items: center;"> <span class="header__address">ул. Красноармейская, 13/95</span> <a class="phoneClick header__phone" style="padding-right: 15px;" href="tel:+78633033079">+7 (863) 303-307-9 </a> <a class="btn callBack-btn waves-effect waves-light text-uppercase modal-trigger header__btn" href="#">Бесплатная консультация</a> </div>
                        <div class="header__menu-wrapper">
                            <ul id="top__menu_id" class="list-inline header__menu">
                                <li><a href="#" class="root-item scroll-nav">Клиника</a></li>
                                <li><a href="#" class="root-item scroll-nav">Услуги</a></li>
                                <li><a href="#" class="root-item scroll-nav">Сотрудники</a></li>
                                <li><a href="#" class="root-item scroll-nav">Отзывы</a></li>
                                <li><a href="#" class="root-item scroll-nav">Пример работы</a></li>
                                <li><a href="#" class="root-item scroll-nav">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                    <a href="tel:+78633033041" class="hamburger hamburger-right-phone phoneMobileClick waves-effect waves-light phoneClick">
                        <div class="hamburger-box"><i class="material-icons">phone</i></div>
                    </a>
                </div>
            </div> <a href="#rassrochka" class="bsToAction">Внимание! Уникальные условия!<span class="hideOnMobile"> Оказываем услуги с рассрочкой платежа!</span></a> </header>
        <div class="section1 container-fluid">
            <div class="main__slider">
                <div class="slide main__background main__background-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="font-weight-bold color-white text-center text-xl-right">Стоматологическая клиника <span class="dark-red">"Грация"</span></h1>
                                <p class="color-white text-center text-xl-right">Инновационные методики лечения и протезирования зубов</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide main__background main__background-2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="font-weight-bold color-white text-center text-xl-right"><span class="dark-red">Колоссальный опыт</span> и многолетняя практика</h1>
                                <p class="color-white text-center text-xl-right">Расположены в самом центре Ростова</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Изменил порядок -->
            <div class="container section1__block">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="section1__block-item">
                            <div class="section1__block-imgWrap"> <span>1</span> </div>
                            <div class="section1__block-text">
                                <div class="h6 font-weight-bold ">Опытные врачи</div>
                                <p>В числе наших специалистов – врачи с более чем 15-летним опытом работы!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="section1__block-item">
                            <div class="section1__block-imgWrap"> <span>2</span> </div>
                            <div class="section1__block-text">
                                <div class="h6 font-weight-bold ">Безболезненное лечение</div>
                                <p>Специальная техника анестезии обеспечивает полностью безболезненное лечение</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="section1__block-item">
                            <div class="section1__block-imgWrap"> <span>3</span> </div>
                            <div class="section1__block-text">
                                <div class="h6 font-weight-bold ">Работаем в выходные!</div>
                                <p>Дежурный хирург работает по субботам! Воскресенье &mdash; по предварительному звонку.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section2 section-dark-gray container-fluid pbt-60 pt-80 " id="servise">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="h2 text-center font-weight-bold">Клиника «Грация» работает <span class="dark-red">с 2009 года</span></div>
                        <p class="text-center delimetr pbr-1">Все виды лечения</p>
                    </div>
                </div>
                <div class="row service__blocks">
                    <div class="col-sm-6 col-lg-4 service__item wow fadeInLeft">
                        <span class="service__item-text-wrapper service-1-btn callBack-btn"> <img src="images/1.jpg" alt="" class="img-fluid">
                            <div class="service__title h5 bold">Ортодонтия</div>
                            <div class="service__param">Исправление прикуса, установка брекетов</div>
                        </span>
                    </div>
                    <div class="col-sm-6 col-lg-4 service__item wow fadeInLeft">
                        <span class="service__item-text-wrapper service-2-btn callBack-btn"> <img src="images/2.jpg" alt="" class="img-fluid">
                            <div class="service__title h5 bold">Белоснежная улыбка</div>
                            <div class="service__param">Чистка, отбеливание и реставрация зубов</div>
                        </span>
                    </div>
                    <div class="col-sm-6 col-lg-4 service__item wow fadeInLeft">
                        <span class="service__item-text-wrapper service-3-btn callBack-btn"> <img src="images/3.jpg" alt="" class="img-fluid">
                            <div class="service__title h5 bold">Терапия</div>
                            <div class="service__param">Лечение кариеса, пульпита и периодонтита</div>
                        </span>
                    </div>
                    <div class="col-sm-6 col-lg-4 service__item wow fadeInLeft">
                        <span class="service__item-text-wrapper service-4-btn"> <img src="images/4.jpg" alt="" class="img-fluid">
                            <div class="service__title h5 bold">Ортопедия, импланты</div>
                            <div class="service__param">Металлокерамические коронки. Мостовидные протезы. Зубные импланты. Частично-съемное протезирование. Керамические вкладки, виниры, люминиры. Бюгельные протезы</div>
                        </span>
                    </div>
                    <div class="col-sm-6 col-lg-4 service__item wow fadeInLeft">
                        <span class="service__item-text-wrapper service-5-btn callBack-btn"> <img src="images/5.jpg" alt="" class="img-fluid">
                            <div class="service__title h5 bold">Хирургия</div>
                            <div class="service__param">Прилагаем все усилия, чтобы ваши зубы остались на своих местах.Устранение опухолей, дефектов и заболеваний. Решаем самые сложные задачи</div>
                        </span>
                    </div>
                    <div class="col-sm-6 col-lg-4 service__item wow fadeInLeft">
                        <span class="service__item-text-wrapper service-6-btn callBack-btn"> <img src="images/6.jpg" alt="" class="img-fluid">
                            <div class="service__title h5 bold">Пародонтология</div>
                            <div class="service__param">Пародонтит локального типа. Лоскутная операция. Шинирование зубов</div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="section3 section-gray-light pbt-60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 section3__gallery-wrap">
                        <div class="h2 font-weight-bold">Самые комфортные условия!</div>
                        <p>Проводим лечение на европейском оборудовании высочайшего класса. Дружный коллектив стоматологов, каждый из которых владеет своей профессией в совершенстве.</p>
                        <div class="section3__gallery row no-gutters">
                            <div class="col-sm-6">
                                <a href="images/gl-2-364x540-2.jpg" data-fancybox="group2"> <img src="images/gl-2-364x540.jpg" /> </a>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <a href="images/gl-2-264x264-2.jpg" data-fancybox="group2"> <img src="images/gl-2-264x264.jpg" /> </a>
                                </div>
                                <div>
                                    <a href="images/gl-2-100x100-2.jpg" data-fancybox="group2"> <img src="images/gl-2-100x1001.jpg" /> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <div class="h2 font-weight-bold">Есть сомнения?</div>
                        <p>Свяжитесь с нами и мы бесплатно проконсультируем вас по всем вопросам, касающихся преимуществ лечения у нас!</p>
                        <div class="row">
                            <form id="section3__form" class="col-12 form3" action="ajax/send.php">
                                <div class="row no-gutters">
                                    <div class="input-field col-md-12">
                                        <input id="last_name" required="" placeholder="Имя" name="name" type="text" class="validate"> </div>
                                </div>
                                <input type="text" class="hide" name="phone">
                                <div class="row no-gutters">
                                    <div class="input-field col-md-12">
                                        <div class="form__error" id="phn"></div>
                                        <input required="" id="phone" placeholder="Телефон" name="phn" type="text" class="validate phone"> </div>
                                </div>
                                <div class="row no-gutters">
                                    <div class="input-field col-md-12">
                                        <textarea id="textarea" placeholder="Ваш комментарий" name="comment" class="materialize-textarea"></textarea>
                                    </div>
                                </div>
                                <p class="text-center small">Нажимая на кнопку, вы соглашаетесь с <a href="#" class="underline">политикой конфиденциальности</a></p>
                                <br>
                                <div class="row no-gutters justify-content-center mb-40">
                                    <button class="btn  btn-large  waves-effect waves-light" type="submit" name="action">Оформить заявку</button>
                                </div>
                                <div class="row no-gutters justify-content-center">
                                    <p class="text-center text-uppercase font-weight-bold col-12"> А так же можете позвонить нам: </p>
                                    <div class="section3__phone-wrapper"> <a class="phoneClick col-12" href="tel:+78633033079">+7 (863) 303-307-9 </a> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section7 section-gray-light container-fluid pbt-60 pt-80 " id="employees">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mb-30">
                        <div class="h2 text-center font-weight-bold delimetr pbr-1">Для вас стараются специалисты:</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 mb-30"><img class="img-fluid" src="images/photo1.jpg" alt="Стоматолог 1"></div>
                        <div class="col-md-4 col-sm-6 mb-30"><img class="img-fluid" src="images/photo2.jpg" alt="Стоматолог 2"></div>
                        <div class="col-md-4 col-sm-6 mb-30"><img class="img-fluid" src="images/photo3.jpg" alt="Стоматолог 3"></div>
                        <div class="col-md-4 col-sm-6 mb-30"><img class="img-fluid" src="images/photo4.jpg" alt="Стоматолог 4"></div>
                        <div class="col-md-4 col-sm-6 mb-30"><img class="img-fluid" src="images/photo5.jpg" alt="Стоматолог 5"></div>
                        <div class="col-md-4 col-sm-6 mb-30"><img class="img-fluid" src="images/photo6.jpg" alt="Стоматолог 6"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="action" class="section5 container-fluid pbt-60 parallax-container">
            <div class="parallax" data-src="images/step-bg.jpg" data-pos-x="left" data-parallax></div>
            <div class="container color-white">
                <div class="row">
                    <div class="col-md-12">
                        <div class="h2 text-center font-weight-bold"><span style="color:#FFFF61;">Запишитесь на приём</span>
                            <br> и получите полный план лечения!</div>
                        <div class="row">
                            <form id="section3__form" class="col-12 col-lg-8 offset-lg-2 mt-30 form4" action="ajax/send.php">
                                <div class="row no-gutters">
                                    <div class="input-field col-md-7">
                                        <input required="" id="phone" placeholder="Ваш номер телефона" type="text" name="phn" class="validate phone"> </div>
                                    <input type="text" class="hide" name="phone">
                                    <button class="btn btn-large waves-effect waves-light col-md-4 offset-md-1" type="submit" name="action">Записаться</button>
                                </div>
                            </form>
                            <div class="col-12">
                                <p class="text-center small">Нажимая на кнопку, вы соглашаетесь с <a href="#" class="color-white underline">политикой конфиденциальности</a></p>
                                <br>
                                <div class="row no-gutters justify-content-center">
                                    <p class="text-center text-uppercase font-weight-bold col-12 fs-1-7r"> Или звоните по телефону: </p>
                                    <div class="section3__phone-wrapper fs-2-5r"> <a class="phoneClick color-white underline col-12" href="tel:+78633033079">+7 (863) 303-307-9 </a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pbt-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pbr-1">
                        <div class="h2 text-center pbr-1 delimetr font-weight-bold">Наши <span class="dark-red">лицензии</span></div>
                    </div>
                    <div class="col-md-12 text-center">
                        <div class="row">
                            <div class="col-md-4 pb-10">
                                <a class="" data-fancybox="lic" href="/images/sert/lic-1.jpg"> <img class="img-fluid" src="/images/sert/lic-1-s.jpg" alt=""> </a>
                                <a class="" data-fancybox="pr" href="/images/sert/lic-2.jpg"></a>
                            </div>
                            <div class="col-md-4 pb-10">
                                <a class="" data-fancybox="pr" href="/images/sert/pr-1.jpg"> <img class="img-fluid" src="/images/sert/pr-1-s.jpg" alt=""> </a>
                            </div>
                            <div class="col-md-4 pb-10">
                                <a class="" data-fancybox="pril" href="/images/sert/pril-1.jpg"> <img class="img-fluid" src="/images/sert/pril-1-s.jpg" alt=""> </a>
                                <a class="" data-fancybox="pril" href="/images/sert/pril-2.jpg"></a>
                                <a class="" data-fancybox="pril" href="/images/sert/pril-3.jpg"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sectionY section-dark-gray pbt-60" id="reviews">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pbr-1">
                        <div class="h2 text-center font-weight-bold">Отзывы <span class="dark-red">наших</span> пациентов</div>
                        <p class="text-center delimetr pbr-1">Ниже представлены реальные отзывы пациентов</p>
                    </div>
                    <div class="col-md-6 offset-md-3 pbr-1 review__wrapper">
                        <div class="review__inner">
                            <div class="review__text">
                                <a class="yv__item mb-0 js-modal-btn" data-video-id="fG4XqZLsKIc" href="#modalYV"> <img class="img-fluid" src="images/review1.jpg" alt=""> </a>
                            </div>
                            <div class="review__author"> </div>
                        </div>
                    </div>
                    <div class="col-md-6 pbr-1 review__wrapper">
                        <div class="review__inner">
                            <div class="review__text"> Клиника «Грация» это точно та клиника, которую я посоветую всем родным и знакомым. А Багдасарян Э.С. это точно тот врач, которому можно довериться. На первом же приёме у Эдгара Сергеевича мне стало понятно, что я попала к большому профессионалу, очень тактичному и внимательному. Доктор составил план лечения, подробно все о нём рассказал и предложил разные пути решения. </div>
                            <div class="review__tail"></div>
                            <div class="review__author"> Марина Куркина </div>
                        </div>
                    </div>
                    <div class="col-md-6 pbr-1 review__wrapper">
                        <div class="review__inner">
                            <div class="review__text"> Огромная благодарность всем сотрудникам и особенно Эдгару Сергеевичу Багдасаряну клиники за профессионализм, настоящую заботу о пациентах, золотые руки. Желаю всего наилучшего! Лечите зубки в «Грации», вам очень понравится. </div>
                            <div class="review__tail"></div>
                            <div class="review__author"> Татьяна Мельникова </div>
                        </div>
                    </div>
                    <div class="col-md-6 pbr-1 review__wrapper">
                        <div class="review__inner">
                            <div class="review__text"> Клиника Грация работает более 10 лет. Врачи клиники Багдасарян Эдгар Сергеевич(терапевт) и Хуршудян Анушаван Рубенович(ортопед)-профессионалы самого высокого класса. Слышала много историй, когда они оказывали помощь даже в таких сложных случаях, в которых многие клиники отказывались от лечения. Знаю много благодарных пациентов, обслуживающихся семьями, на протяжении многих лет. Высокое качество и безопасность оказываемых услуг, стоит в клинике на первом месте! Буду рекомендовать эту клинику всем своим родственникам и друзьям. </div>
                            <div class="review__tail"></div>
                            <div class="review__author"> Наталья </div>
                        </div>
                    </div>
                    <div class="col-md-6 pbr-1 review__wrapper">
                        <div class="review__inner">
                            <div class="review__text"> Хочу выразить благодарность сотрудникам стоматологической клиники «Грация», а в частности Багдасаряну Э.С., за доброжелательное отношение, внимание к своим пациентам, качественное обслуживание, профессионализм. Уже многие годы наша семья пользуется услугами клиники и доверяет свою улыбку высококвалифицированным специалистам! С уважением, Одинцова И. </div>
                            <div class="review__tail"></div>
                            <div class="review__author"> Ира Одинцова </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rassrochka" class="section5 container-fluid pbt-60 parallax-container">
            <div class="parallax" data-src="images/step-bg.jpg" data-pos-x="left" data-parallax></div>
            <div class="container color-white">
                <div class="row">
                    <div class="col-md-12">
                        <div class="h2 text-center font-weight-bold">Оказываем <span class="dark-red">услуги</span> с рассрочкой платежа!</div>
                        <p class="text-center delimetr pbr-1">Узнать подробности:</p>
                        <div class="row">
                            <form id="section3__form" class="col-12 col-lg-8 offset-lg-2 mt-30 form5" action="ajax/send.php">
                                <div class="row no-gutters">
                                    <div class="input-field col-md-7">
                                        <input required="" id="phone" placeholder="Ваш номер телефона" type="text" name="phn" class="validate phone"> </div>
                                    <input type="text" class="hide" name="phone">
                                    <button class="btn btn-large waves-effect waves-light col-md-4 offset-md-1" type="submit" name="action">Связаться</button>
                                </div>
                            </form>
                            <div class="col-12">
                                <p class="text-center small">Нажимая на кнопку, вы соглашаетесь с <a href="#" class="color-white underline">политикой конфиденциальности</a></p>
                                <br>
                                <div class="row no-gutters justify-content-center">
                                    <p class="text-center text-uppercase font-weight-bold col-12 fs-1-7r"> Или звоните по телефону: </p>
                                    <div class="section3__phone-wrapper fs-2-5r"> <a class="phoneClick color-white underline col-12" href="tel:+78633033079">+7 (863) 303-307-9</a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section6 container-fluid pbt-60 pt-80 " id="appointment">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mb-30">
                        <div class="h2 text-center font-weight-bold">Сделаем вашу улыбку белоснежной!</div>
                        <p class="text-center delimetr pbr-1">Оставьте заявку или получите бесплатную консультацию по телефону</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 footer__list">
                        <div class="footer__list-item mb-15"><i class="material-icons pr-10">email</i><span>Напишите на почту: <a href="mailto:graciaclinic@gmail.com">graciaclinic@gmail.com  </a></span></div>
                        <div class="footer__list-item mb-15"><i class="material-icons pr-10">phone</i><a class="phoneClick" href="tel:+78633033079">+7&nbsp;(863)&nbsp;303-307-9</a></div>
                        <div class="footer__list-item mb-15">Еще сомневаетесь? Более 90% наших пациентов рекомендую нас своим друзьям и знакомым.</div>
                    </div>
                    <form id="form__footer" class="col-md-6 form6" action="ajax/send.php">
                        <div class="row no-gutters">
                            <div class="input-field input-name col-md-6">
                                <input id="name" required="" placeholder="Имя" type="text" name="name" class="validate"> </div>
                            <div class="input-field col-md-6">
                                <input required="" id="footer__phone" placeholder="Телефон" type="text" name="phn" class="validate phone"> </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="input-field col-md-12">
                                <textarea id="footer__textarea" placeholder="Ваш комментарий" name="comment" class="materialize-textarea"></textarea>
                            </div>
                        </div>
                        <input type="text" class="hide" name="phone">
                        <p class="text-center small ">Нажимая на кнопку, вы соглашаетесь с <a href="#" class="underline">политикой конфиденциальности</a></p>
                        <br>
                        <div class="row no-gutters mb-40">
                            <button class="btn btn-large waves-effect waves-light" type="submit" name="action">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="section5 container-fluid pbt-60 parallax-container">
            <div class="parallax" data-src="images/step-bg.jpg" data-pos-x="left" data-parallax></div>
            <div class="container color-white">
                <div class="row">
                    <div class="col-md-12">
                        <div class="h2 text-center font-weight-bold">Путь к вашей белоснежной улыбке будет выглядеть так:</div>
                        <br> </div>
                </div>
                <div class="row ico__row">
                    <div>
                        <div class="ico n01"><i class="icon-telephone"></i></div>
                        <p>Консультация по телефону</p>
                    </div>
                    <div>
                        <div class="ico n02"><i class="icon-pencil"></i></div>
                        <p>Запись на прием</p>
                    </div>
                    <div>
                        <div class="ico n03"><i class="icon-stethoscope"></i></div>
                        <p>Профессиональный осмотр</p>
                    </div>
                    <div>
                        <div class="ico n04"><i class="icon-edit"></i></div>
                        <p>Составление плана лечения</p>
                    </div>
                    <div>
                        <div class="ico n05"><i class="icon-conversation"></i></div>
                        <p>Обсуждение конечного результата</p>
                    </div>
                    <div>
                        <div class="ico n06"><i class="icon-doctor"></i></div>
                        <p>Лечение согласно плану</p>
                    </div>
                    <div>
                        <div class="ico n07"><i class="icon-happy"></i></div>
                        <p>Ваша прекрасная улыбка</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="example" class="section8 container-fluid pbt-60 pt-80 skill ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mb-30">
                        <div class="h2 text-center font-weight-bold  pbr-1">Примеры нашей работы</div>
                        <p class="text-center delimetr pbr-1">Восстановление зубов молодому человеку, 21 год.
                            <br> Все зубы были <span class="dark-red font-weight-bold">сохранены и восстановлены</span></p>
                    </div>
                </div>
                <div class="row skill-image h2 font-weight-bold">
                    <div class=" col-md-6"><img src="images/skill/before.jpg" alt="">
                        <p>До</p>
                    </div>
                    <div class="  col-md-6"><img src="images/skill/after.jpg" alt="">
                        <p><span class="dark-red">После</span></p>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Не хватает средств? --> 

        <!-- Не хватает средств? КОНЕЦ --> 

        <div class="section5 container-fluid pbt-60 parallax-container" id="contacts">
            <div class="parallax" data-src="images/step-bg.jpg" data-pos-x="left" data-parallax></div>
            <div class="container color-white">
                <div class="row">
                    <div class="col-md-12">
                        <div class="h2 text-center font-weight-bold delimetr pbr-1">Остались <span class="dark-red">вопросы?</span>
                            <br>Оставьте заявку и наш специалист
                            <br>с удовольствием на них ответит</div>
                        <div class="row">
                            <form id="section3__form" class="col-12 col-lg-6 offset-lg-3 mt-30 form7" action="ajax/send.php" method="POST">
                                <div class="row no-gutters">
                                    <div class="input-field col-md-12">
                                        <input required="" id="phone" placeholder="Ваш номер телефона" type="text" name="phn" class="validate phone"> </div>
                                    <input type="text" class="hide" name="phone">
                                    <button class="btn btn-large waves-effect waves-light col-md-12" type="submit" name="action">Получить ответ</button>
                                </div>
                            </form>
                            <div class="col-12">
                                <p class="text-center small">Нажимая на кнопку, вы соглашаетесь с <a href="#" class="color-white underline">политикой конфиденциальности</a></p>
                                <br> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="color-white color-white-a pbt-40">
            <div class="container">
                <div class="row footer-contact">
                    <div class="col-md-4">
                        <p>Стоматологическая клиника "Грация"
                            <br>2005 -
                            <?=date('Y');?>.
                                <br>Все права защищены</p> <a href="mailto:graciaclinic@gmail.com  ">graciaclinic@gmail.com  </a> &nbsp; &nbsp; <a href="https://www.instagram.com/graciaclinic_stomatologia/" target="_blank"><b>I</b>nstagram</a> </div>
                    <div class="col-md-5">
                        <p class="footer-call"> <a class="phoneClick color-white underline col-12" href="tel:+78633033079">+7&nbsp;(863)&nbsp;303-307-9</a> </p>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-copyright h-100">
                            <div class="row h-100">
                                <div class="col-md-12">
                                    <a href="http://www.bondsoft.ru/" target="_blank">
                                        <svg viewBox="0 0 261.12 44.19" class="icon_logo" width="133" height="22px">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/logo_bs.svg#logo_bs"></use>
                                        </svg> <span>создание продвижение</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/waves.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/lazyframe.min.js"></script>
    <script src="js/hammer.js"></script>
    <script src="js/jquery.fancybox3.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.mmenu.all.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery-modal-video.min.js"></script>
    <script src="js/jquery.parallax.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/iziModal.min.js"></script>
    
    <script src="js/lib.js"></script>
    <script src="js/main.js"></script>
    
    <div id="callback" class="black">
        <form class="ajax-form-callback" action="ajax/send.php" method="POST">
            <input type="text" required="" placeholder="Имя" name="name">
            <div class="form__error" id="error__name"></div>
            <input required="" id="phone" placeholder="Телефон" name="phn" type="text" class="validate phone">
            <div class="form__error" id="error__phn"></div>
            <input type="hidden" name="source" value="">
            <input class="hide" type="text" name="phone">
            <p class="text-center small">Нажимая на кнопку, вы соглашаетесь с <a href="#" class="underline">политикой конфиденциальности</a></p>
            <br>
            <input type="submit" class="btn button waves-effect waves-light text-uppercase" value="Отправить"> </form>
    </div>
    
    <div id="modal-success"></div>
    <div id="modal-error"></div>
    
    <div class="cback">
        <a class="mobile__cback-href" href="tel:+79255423557"></a>
        <a href="#" class="callOrderForm callBack-btn">
            <div class="cback-circle fn1"></div>
            <div class="cback-circle fn2"></div>
            <div class="cback-circle cback-circle--phone">
                <!-- <i class=''></i> --><i class="fa fa-phone phone-icon" aria-hidden="true"></i> <span style="display:none">КНОПКА<br>СВЯЗИ</span> </div>
        </a>
    </div>
    
</body>

</html>