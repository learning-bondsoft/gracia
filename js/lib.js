var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();
$(document).ready(function () {
    Waves.init();
    $("#sticker").sticky({
        topSpacing: 0
    });
    $(document).on('click', '.bsToAction', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var scrollToPx = $(href).offset().top - 113;
        console.log();
        $("html, body").animate({
            scrollTop: scrollToPx
        }, 400);
    });
    var mobileMenuObj = $(".header__menu").clone().removeClass("header__menu list-inline").removeAttr('id');
    mobileMenuObj = $("<div id='my-menu'></div>").append(mobileMenuObj);
    $("#body-inner").append(mobileMenuObj);
    var $menu = $('#my-menu');
    $menu.mmenu({
        drag: {
            menu: {
                open: ($.mmenu.support.touch ? true : false)
                , width: {
                    perc: 0.8
                }
            }
        }
        , navbar: {
            title: "<a href=\"/\" class=\"mLogo\">Меню</a>"
        }
        , "extensions": [
	    "fx-menu-slide"
	    , "fx-panels-zoom"
    ]
    });
    var api = $("#my-menu").data("mmenu");
    $("#hamburger").click(function () {
        api.open();
    });
    $("#hamburger").click(function () {
        api.close();
    });
    api.bind('open:finish', function () {
        $('html').addClass('slideout-open');
    });
    api.bind('close:before', function () {
        $('html').removeClass('slideout-open');
    });
    $("[data-fancybox]").fancybox({});
    //lazyframe
    let elements = $('.lazyframe');
    lazyframe(elements, {
        apikey: undefined
        , debounce: 250
        , lazyload: false
    , });
    $(".js-modal-btn").modalVideo();
    //hover menu
    var TimerInterval;
    $(".root-item-wrp, .current").on("mouseover", function () {
        $(this).addClass("jshover");
        $(".root-item-wrp, .current").not(this).removeClass("jshover");
        clearTimeout(TimerInterval);
    });
    $(".root-item-wrp, .current").on("mouseleave", function () {
        var that = this;
        var TimerInterval = setTimeout(function () {
            $(that).removeClass("jshover");
        }, 600);
    });
    $("#callback").iziModal({
        title: 'Заказать обратный звонок'
        , subtitle: 'Перезвоним как можно скорее'
        , headerColor: '#2b2b2b'
        , icon: null
        , background: null
        , iconText: null
        , /*iconColor: '',*/
        rtl: false
        , width: 600
        , top: null
        , bottom: null
        , borderBottom: true
        , padding: 18
        , radius: 0
        , /*fullscreen: true,*/
        closeOnEscape: true
        , closeButton: true
        , zindex: 999
    , });
    $(document).on('click', '.callBack-btn', function (event) {
        event.preventDefault();
        var source = $(this).data('source');
        $('#callback input[name="source"]').val(source);
        $('#callback').iziModal('open');
    });
    $("#modal-success").iziModal({
        title: "Ваше сообщение отправлено"
        , subtitle: "Мы обязательно с Вами свяжемся"
        , icon: null
        , headerColor: '#00af66'
        , width: 600
        , timeout: 4000
        , timeoutProgressbar: true
        , transitionIn: 'fadeInUp'
        , transitionOut: 'fadeOutDown'
        , bottom: 0
        , loop: true
        , pauseOnHover: true
    });
    $("#modal-error").iziModal({
        title: "Произошла ошибка"
        , subtitle: "Попробуйте отправить ваще обращение снова"
        , icon: null
        , headerColor: '#BE0F18'
        , width: 600
        , timeout: 4000
        , timeoutProgressbar: true
        , transitionIn: 'fadeInUp'
        , transitionOut: 'fadeOutDown'
        , bottom: 0
        , loop: true
        , pauseOnHover: true
    });
    $(document).on('click', '.callBack-btn', function (event) {
        event.preventDefault();
        var source = $(this).data('source');
        $('#callback input[name="source"]').val(source);
        $('#callback').iziModal('open');
    });
})