//Формирование карты

                function initMap() {
                    var contentString = '<div id="content">'+
                                        '<div id="siteNotice">'+
                                        '</div>'+
                                        '<h1 id="firstHeading" class="firstHeading">Клиника «Грация»</h1>'+
                                        '<div id="bodyContent">'+
                                        '<p>Проводим лечение на европейском оборудовании высочайшего класса. '+
                                        'Дружный коллектив стоматологов, каждый из которых владеет своей профессией в совершенстве.</p>'+    
                                        '<p><b>Веб-сайт:</b> <a href="http://gracia.bs-demo.ru//" target="_blank">gracia.bs-demo.ru</a>'+
                                        '</p>'+
                                        '<p><b>Адрес:</b> Красноармейская улица, 13/95, Ростов-на-Дону'+
                                        '</p>'+
                                        '<p><b>Телефон:</b> <a href="tel:+78633033079">+7 (863) 303-307-9</a>'+
                                        '</p>'+
                                        '</div>'+
                                        '</div>';
                    
                    
                    var uluru = {lat: 47.226340, lng: 39.701705};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 17,
                        center: uluru,
                        					styles: mapStyler
                    });
                    var marker = new google.maps.Marker({
                        position: uluru,
                        map: map,
                        title: 'Клиника «Грация»',
                        icon: 'images/logo-map.png'
                    });
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString,
                        maxWidth: 400
                    });
                    marker.addListener('click', function() {
                        infowindow.open(map, marker);
                    });
                }




//Стиль для карты


var mapStyler = [

  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#b5cbe4"
            }
        ]
    },
    {
        "featureType": "landscape",
        "stylers": [
            {
                "color": "#efefef"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#83a5b0"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#bdcdd3"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e3eed3"
            }
        ]
    },
    {
        "featureType": "administrative",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 50
            }
        ]
    },
    {
        "featureType": "road"
    },
    {
        "featureType": "poi.park",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "lightness": 10
            }
        ]
    },
    {},
    {
        "featureType": "road",
        "stylers": [
            {
                "lightness": 20
            }
        ]
    }
]
