$(document).ready(function () {
let valid = {
        errorElement: "div"
        , rules: {
            name: {
                required: true
                , minlength: 2
            }
            , phn: {
                required: true
                , minlength: 18
            }
        , }
        , messages: {
            name: {
                required: "(это обязательное поле)"
                , minlength: "(введите имя полностью)"
            }
            , phn: {
                required: "(это обязательное поле)"
                , minlength: "(введите номер полностью)"
            }
        , }
        , submitHandler: function (form) {
            //Отправка формы
            var formData = $(form).serialize()
                , action = $(form).attr('action');
            $.ajax({
                method: 'POST'
                , data: formData
                , dataType: 'json'
                , url: action
                , success: function (data) {
                    
                    if (data.success == 1) {
                        $("#callback").iziModal('close');
                        $('#modal-success').iziModal('open');
                    }
                    else {
                        $("#callback").iziModal('close');
                        $('#modal-error').iziModal('open');
                    }
                }
                , error: function (error) {
                    $("#callback").iziModal('close');
                    $('#modal-error').iziModal('open');
                }
            });
            return false;
        }
    }

    $('.ajax-form-callback').validate(valid);
    $('input.phone').mask('+7 (000) 000-00-00');
    
    
    $('.main__slider').slick({
        dots: false
        , arrow: false
        , autoplay: true
        , autoplaySpeed: 5000
        , mobileFirst: true
        , pauseOnHover: false
    });
});